<?php

use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;

/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 19.4.2018
 * Time: 16:38
 */
class LoginForm extends BaseForm
{

    /**
     * Create form
     * @return Form
     */
    public function create(): Form
    {
        $form = $this->createBaseForm();
        $form->addEmail('email', ' ')
            ->setAttribute('autocomplete', 'on')->setAttribute('placeholder', 'forms.email');

        $form->addPassword('heslo', '')
            ->setAttribute('autocomplete', 'on')->setAttribute('placeholder', 'forms.password');

        $form->addCheckbox('stayLogged', 'forms.stayLogged');

        $form->addSubmit('submit', 'forms.submits.login');
        $form->addButton('facebookLogin', 'forms.submits.facebookLogin');
        $form->addButton('googleLogin', 'forms.submits.googleLogin');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     * @return void
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Security\AuthenticationException
     */
    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        try {
            $this->user->login($values['email'], $values['heslo']);
        } catch (AuthenticationException|\Error $exception) {
            $this->_this->flashMessage($exception->getMessage(), 'error');
            $form->addError('forms.errors.incorrectPassword');
        }
        if ($this->user->isLoggedIn()) {
            $this->_this->redirect('Homepage:');
        } else {
            $form->addError('forms.errors.incorrectPassword');
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     * @return void
     */
    public function formValidate(Form $form, ArrayHash $values): void
    {
        // TODO: Implement formValidate() method.
    }


}