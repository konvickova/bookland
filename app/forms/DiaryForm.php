<?php

use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;


/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 2.7.2018
 * Time: 12:58
 */
class DiaryForm extends BaseForm
{

    public function create(): Form
    {
        $form = $this->createBaseForm();
        $form->addText('nazevKnihy', ' ')->setAttribute('placeholder', 'Název knihy')->setRequired();
        $form->addText('autor', ' ')->setAttribute('placeholder', 'Autor')->setRequired();
        $form->addText('rokVydani', '  ')->setAttribute('placeholder', 'Rok vydání');
        $form->addText('ilustrator', ' ')->setAttribute('placeholder', 'ilustrátor');
        $form->addText('nakladatel', ' ')->setAttribute('placeholder', 'Nakladatel');
        $form->addTextArea('obsah', ' ')->setAttribute('placeholder', 'Obsah')->setRequired();
        $form->addTextArea('citaty', ' ')->setAttribute('placeholder', 'Oblíbený citát');
        $form->addTextArea('dojmy', ' ')->setAttribute('placeholder', 'Dojmy a jine poznamky ke knize');

        $form->addSubmit('submit', 'Přidat');

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {

    }


}