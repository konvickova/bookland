<?php

use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\ArrayHash;

/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 19.4.2018
 * Time: 16:39
 */
class RegistrationForm extends BaseForm
{
    /**
     * Create form
     */
    public function create()
    {
        $form = $this->createBaseForm();
        $form->addText('jmeno', 'jmeno')->setAttribute('placeholder', 'jméno')
            ->setRequired('Zadejte prosím jméno');

        $form->addText('prijmeni', 'prijmeni')->setAttribute('placeholder', 'příjmení')
            ->setRequired('Vyplňte prosím příjmení');

        $form->addSelect('pohlavi', '', ['f' => ' žena', 'm' => ' muž']);

        $form->addEmail('email', 'forms.email')->setAttribute('placeholder', 'e-mail')
            ->setAttribute('autocomplete', 'on')->setRequired('Vyplňte prosím e-mail');

        $form->addPassword('heslo', 'forms.password')
            ->setAttribute('placeholder', 'heslo')->setRequired('Vyplňte prosím heslo')
            ->addRule(FORM::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků', 8);

        $form->addPassword('hesloZnovu', 'forms.password')->setAttribute('placeholder', 'heslo znovu')
            ->setRequired('Vyplňte prosím heslo pro potvrzení');

        $form->addText('narozeniny', 'Datum narozeni')->setAttribute('type', 'date');

        $form->addCheckbox('agree', ' Souhlasim s podminkami.')
            ->addRule(Form::EQUAL, 'Pro dokončení registrace musíte souhlasit')
            ->setRequired('Pro dokončení registrace musíte souhlasit s podmínkami.');

        $form->addSubmit('submit', 'forms.submits.registration');

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        $mail = new Message;

        $mail->setFrom($this->context->getParameters()['companyEmail'])
            ->addTo('')
            ->setSubject('Registrace do Booklandu')
            ->setHtmlBody((new \Latte\Engine())->renderToString(__DIR__ . './templates/Emails/registration.latte', (array)$values));


        $this->flashMessage('Registrace proběhla úspěšně.');
        $this->redirect('Homepage:');


    }


}