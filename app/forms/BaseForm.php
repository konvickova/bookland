<?php

use App\Presenters\BasePresenter;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Nette\Security\User;

/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 19.4.2018
 * Time: 14:14
 */
class BaseForm
{
    /**
     * @var Translator
     */
    public $translator;

    protected $_this;

    /**@var User */
    protected $user;


    /**
     * BaseForm constructor.
     * @param BasePresenter $_this
     */
    public function __construct(BasePresenter $_this)
    {
        $this->translator = $_this->translator;
        $this->_this = $_this;
        $this->user = $_this->getUser();
    }

    /**
     * @param $name
     * @return object
     */
    protected function getService($name)
    {
        if (!$this->_this->context->hasService($name)) {
            exit("Service $name not exist!<br>");
        }
        return $this->_this->context->getService($name);
    }

    protected function createBaseForm($includeStyles = true)
    {
        /** @var Translator $translator */
        $translator = $this->translator;
        $form = new class extends Form
        {
            public function cleanValues()
            {
                foreach ($this->getComponents() as $name => $control) {
                    $control->setValue(null);
                }
            }
        };

        //$form->getElementPrototype()->class('ajax');
        $form->setTranslator($translator);
        $form->onSuccess[] = [$this, 'formSucceeded'];
        $form->onValidate[] = [$this, 'formValidate'];
        $form->addProtection($translator->translate('messages.error.timeout'));

        if ($includeStyles) {
            $renderer = $form->getRenderer();
            $renderer->wrappers['controls']['container'] = null;
            $renderer->wrappers['pair']['container'] = 'div class=form-group';
            $renderer->wrappers['pair']['.error'] = 'has-error';
            $renderer->wrappers['control']['container'] = 'div class=col-sm-11';
            $renderer->wrappers['label']['container'] = 'div class="col-sm-1 control-label"';
            $renderer->wrappers['control']['description'] = 'span class=help-block';
            $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
            $form->getElementPrototype()->class('form-horizontal ajax');
            $form->onRender[] = function ($form) {
                /** @var Form $form */
                foreach ($form->getControls() as $control) {
                    $type = $control->getOption('type');
                    if ($type === 'button') {
                        $control->getControlPrototype()->addClass('ladda-button btn btn-' . (empty($usedPrimary) ? 'primary' : 'default'));
                        $control->setAttribute('data-style="contract"');
                        $usedPrimary = true;
                    } elseif (in_array($type, ['text', 'textarea'], true)) {
                        $control->getControlPrototype()->addClass('form-control');
                    } elseif (in_array($type, ['select'], true)) {
                        $control->getControlPrototype()->addClass('js-example-basic-single form-control');
                    } elseif (in_array($type, ['checkbox', 'radio'], true)) {
                        $control->getSeparatorPrototype()->setName('div')->addClass('i-checks');
                    }
                }
            };
        }
        return $form;
    }


}