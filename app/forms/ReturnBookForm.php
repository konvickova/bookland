<?php

use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 19.4.2018
 * Time: 16:40
 */
class ReturnBookForm extends BaseForm
{

    /**
     * Create form
     */
    public function create()
    {
        $form = $this->createBaseForm();

        $form->addSelect('nazevKnihy', '', ['f' => ' abc', 'm' => ' def'])->setAttribute('placeholder', 'Název knihy');
        $form->addText('misto', ' ')->setAttribute('placeholder', 'Nová poloha');

        $form->addSubmit('submit', 'Vratit');

        return $form;

    }


    public function formSucceeded(Form $form, ArrayHash $values): void
    {

    }


}