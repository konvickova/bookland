<?php

use Nette\Security as NS;


class Auth implements NS\IAuthenticator
{
    public $database;

    function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('users')
            ->where('email', $username)->fetch();

        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }

        if ($row->heslo !== md5($password)) {
            throw new NS\AuthenticationException('Invalid password.');
        }
        $data = $row->toArray();
        unset($data['heslo']);
        return new NS\Identity($row->id, $row->role, [$data]);
    }
}