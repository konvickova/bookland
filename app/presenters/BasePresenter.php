<?php

namespace App\Presenters;

use App\Repository\BooksRepository;
use Nette;
use Nette\Security\Permission;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /** @var Nette\Database\Context */
    private $database;

    /** @persistent */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /** @var \Auth @inject */
    public $auth;

    /** @var BooksRepository @inject */
    public $booksRepository;

    /**
     * BasePresenter constructor.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct();
        $this->database = $database;
    }

    /**
     * @throws Nette\Application\ForbiddenRequestException
     */
    protected function startup()
    {
        parent::startup();
        $acl = new Nette\Security\Permission;

// definujeme role
        $acl->addRole('guest')
            ->addRole('registered', 'guest')// registered dědí od guest
            ->addRole('admin', 'registered'); // registered dědí od guest

        $acl->addResource('Homepage')
            ->addResource('Authors')
            ->addResource('Books')
            ->addResource('Books:detail')
            ->addResource('Books:return')
            ->addResource('Categories')
            ->addResource('Diary')
            ->addResource('Diary:userDiary')
            ->addResource('Users')
            ->addResource('Users:profile')
            ->addResource('Users:list')
            ->addResource('Sign')
            ->addResource('Registration')
            ->addResource('Categories:list')
            ->addResource('layout:menu');

// nejprve nikdo nemůže dělat nic

// nechť guest může prohlížet obsah veřejné části, hlasovat v anketách
        $acl->allow('guest', ['Homepage', 'Books', 'Authors', 'Sign', 'Registration'], 'View');
        $acl->allow('registered', ['Users', 'Users:profile', 'Diary', 'Diary:userDiary'], 'View');
        $acl->allow('admin', ['Users:list', 'Categories', 'Categories:list', 'layout:menu'], 'View');

// registrovaný dědí právo od guesta, ale má i právo komentovat
        //$acl->allow('registered', 'comment', 'add');

// administrátor může prohlížet a editovat cokoliv
        // $acl->allow('administrator', Permission::ALL, ['view', 'edit', 'add']);

        $this->getUser()->setAuthorizator($acl);
        if (!$this->getUser()->isAllowed($this->presenter->getName(), 'View')) {
            throw new Nette\Application\ForbiddenRequestException;
        }
    }

    function beforeRender()
    {
        parent::beforeRender(); //
        $this->template->lastBooks = $this->booksRepository->getLastBooks(2);
    }

}
