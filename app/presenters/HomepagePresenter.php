<?php

namespace App\Presenters;

use App\Repository\CategoriesRepository;
use App\Repository\BooksRepository;
use App\Repository\UsersRepository;


class HomepagePresenter extends BasePresenter
{
    /** @var CategoriesRepository @inject */
    public $categoriesRepository;

    /** @var BooksRepository @inject */
    public $booksRepository;

    /** @var UsersRepository @inject */
    public $usersRepository;



    public function renderDefault($page = 1)
    {
        $this->template->page = $page;

        $this->template->categoriesList = $this->categoriesRepository
            ->findAll()
            ->order('nazev ASC')
            ->limit(10)
            ->fetchAll();

        $this->template->countOfBooks = $this->booksRepository->getCountOfBooks();

        $this->template->countOfUsers = $this->usersRepository->getCountOfUsers();

        $this->template->countOfCategories = $this->categoriesRepository->getCountOfCategories();

    }

    public function actionLogOut()
    {
        $this->user->logout();
        $this->redirect(200, 'Homepage:');
    }

}
