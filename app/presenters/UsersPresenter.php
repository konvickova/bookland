<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 8.4.2018
 * Time: 15:00
 */

namespace App\Presenters;


use App\Repository\UsersRepository;

class UsersPresenter extends BasePresenter
{

    /** @var UsersRepository @inject */
    public $usersRepository;

    public function renderProfile($id)
    {
        $this->template->userProfile = $this->usersRepository->getById($id);

    }

    public function renderList()
    {
        $this->template->usersList = $this->usersRepository->findAll()->fetchAll();

    }

}