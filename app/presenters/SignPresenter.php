<?php

namespace App\Presenters;

use LoginForm;
use Nette;
use Nette\Application\UI\Form;


class SignPresenter extends BasePresenter
{

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        return (new LoginForm($this))->create();
    }


    public function signInFormSucceeded($form, $values)
    {
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Homepage:');

        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Nesprávný e-mail nebo heslo.');
        }
    }


    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('You have been signed out.');
        $this->redirect('Homepage:');
    }

}
