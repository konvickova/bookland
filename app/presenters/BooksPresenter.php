<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 8.4.2018
 * Time: 13:19
 */

namespace App\Presenters;

use App\Repository\AuthorsRepository;
use App\Repository\BooksRepository;
use App\Repository\CategoriesRepository;
use Nette;
use ReturnBookForm;

class BooksPresenter extends BasePresenter
{

    /** @var AuthorsRepository @inject */
    public $authorsRepository;

    /** @var BooksRepository @inject */
    public $booksRepository;

    /** @var CategoriesRepository @inject */
    public $categoriesRepository;


    public function renderDetail($id)
    {
        $this->template->bookDetail = $this->booksRepository->getById($id);

    }

    public function renderList(int $page = 1)
    {
        $data = $this->booksRepository->getInfoAboutBooks();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($data->count()); // celkový počet článků
        $paginator->setItemsPerPage(8); // počet položek na stránce
        $paginator->setPage($page); // číslo aktuální stránky
        $this->template->paginator = $paginator;
        $this->template->booksInfo = $data->limit($paginator->getLength(), $paginator->getOffset());

        $this->template->categoriesList = $this->categoriesRepository->findAll()
            ->order('nazev ASC')
            ->fetchAll();
    }

    public function renderReturn()
    {


    }

    protected function createComponentReturnBookForm()
    {

        return (new ReturnBookForm($this))->create();
    }


}