<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 8.4.2018
 * Time: 14:37
 */

namespace App\Presenters;


use App\Repository\AuthorsRepository;

class AuthorsPresenter extends BasePresenter
{

    /** @var AuthorsRepository @inject */
    public $authorsRepository;

    public function renderDetail($id)
    {
        $this->template->authorDetail = $this->authorsRepository->getById($id);
    }

    public function renderList()
    {
        $this->template->authorsList = $this->authorsRepository->findAll();


    }


}