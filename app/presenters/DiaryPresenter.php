<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 2.7.2018
 * Time: 11:31
 */

namespace App\Presenters;

use App\Repository\DiaryRepository;
use DiaryForm;

class DiaryPresenter extends BasePresenter
{

    /** @var DiaryRepository @inject */
    public $diaryRepository;

    protected function createComponentDiaryForm()
    {
        return (new DiaryForm($this))->create();

    }

    public function renderUserDiary($id)
    {
        $this->template->diaryRepository = $this->diaryRepository->getById($id);

    }

}