<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 5.7.2018
 * Time: 13:31
 */

namespace App\Presenters;

use RegistrationForm;

class RegistrationPresenter extends BasePresenter
{

    protected function createComponentRegistrationForm()
    {
        return (new RegistrationForm($this))->create();

    }


}