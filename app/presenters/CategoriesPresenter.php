<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 9.4.2018
 * Time: 10:53
 */

namespace App\Presenters;

use App\Repository\CategoriesRepository;

class CategoriesPresenter extends BasePresenter
{

    /** @var CategoriesRepository @inject */
    public $categoriesRepository;

    public function renderList()
    {

        $this->template->categoriesList = $this->categoriesRepository->findAll();
    }


}