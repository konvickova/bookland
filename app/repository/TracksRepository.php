<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 31.3.2018
 * Time: 23:00
 */

namespace App\Repository;


class TracksRepository extends Repository
{
    //10 nejcasteji pujcene
    public function getTop10Books()
    {
        return $this->getTable()->select('books.*, tracks.*, COUNT(*) countOne')->
        joinWhere('books', 'tracks.books_id = books.id')->
        group('books.id')->
        order('countOne')->
        limit(10)->fetchAll();
    }

}