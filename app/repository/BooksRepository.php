<?php
/**
 * Created by PhpStorm.
 * User: Sarlota
 * Date: 30.3.2018
 * Time: 20:48
 */

namespace App\Repository;


class BooksRepository extends Repository
{

    public function getPaginatorLimit($limit, $offset)
    {
        return $this->getTable()->limit($limit, $offset)->order('nazev ASC')->fetchAll();
    }


    //knihy a jejich autori
    public function getInfoAboutBooks()
    {
        return $this->getTable()->
        group('books.id')->
        order('books.nazev ASC');

    }

    public function getCountOfBooks()
    {
        return $this->getTable()->count('id');
    }

    public function getInfoAboutBook($id)
    {
        return $this->getTable();
    }

    public function getLastBooks($limit)
    {
        return $this->getTable()->order('created DESC')->limit($limit)->fetchAll();
    }


}