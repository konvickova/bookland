<?php

namespace App\Repository;

class UsersRepository extends Repository
{


    function getActiveUsers()
    {
        return $this->findBy([])->fetchAll();
    }

    public function registerUser($jmeno, $prijmeni, $heslo, $email)
    {
        return $this->insert($jmeno, $prijmeni, $heslo, $email);
    }

    function getPujceneKnihy()
    {
        return $this->getTable()->
        group('tracks.books_id')->
        order('books.nazev ASC');
    }

    public function getCountOfUsers()
    {
        return $this->getTable()->count('id');
    }


}
