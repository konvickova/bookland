<?php

namespace App\Repository;

use Nette;

/**
 * Provádí operace nad databázovou tabulkou.
 */
abstract class Repository
{

    /** @var Nette\Database\Connection $db */
    protected $db;
    protected $tableName = NULL;

    public function __construct(Nette\Database\Context $db)
    {
        if (!$this->tableName) {
            // název tabulky odvodíme z názvu třídy
            preg_match('#(\w+)Repository$#', get_class($this), $m);
            $this->tableName = lcfirst($m[1]);
        }
        $this->db = $db;
    }

    /**
     * Vrací objekt reprezentující databázovou tabulku.
     * @return Nette\Database\Table\Selection
     */
    protected function getTable()
    {
        return $this->db->table($this->tableName);
    }

    /**
     * Vrací všechny řádky z tabulky.
     * @return Nette\Database\Table\Selection
     */
    public function findAll($orderBy = NULL)
    {
        $selection = $this->getTable();
        if ($orderBy) {
            $selection->order($orderBy);
        }
        return $selection;
    }

    /**
     * Vrací řádky podle filtru, např. array('name' => 'John').
     * @param array $by
     * @param null $orderBy
     * @return Nette\Database\Table\Selection
     */
    public function findBy(array $by, $orderBy = NULL)
    {
        $selection = $this->getTable()->where($by);
        if ($orderBy) {
            $selection->order($orderBy);
        }
        return $selection;
    }

    /**
     * To samé jako findBy akorát vrací vždy jen jeden záznam
     * @param array $by
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function getBy(array $by)
    {
        return $this->findBy($by)->limit(1)->fetch();
    }

    /**
     * Vrací záznam s daným primárním klíčem
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function getById($id)
    {
        return $this->getBy(array('id' => $id));
    }

    /**
     * Pokud je zadané id, udělá update. Jinak udělá insert.
     * @param $id
     * @param $values
     * @return int
     */
    public function persist($id, $values)
    {
        if ($id) {
            $this->update($id, $values);
        } else {
            $id = $this->insert($values);
        }
        return (int)$id;
    }

    public function insert($values)
    {
        $row = $this->getTable()->insert($values);
        return isset($row->id) ? $row->id : NULL;
    }

    public function update($id, $values)
    {
        $row = $this->getTable()->get($id);
        $row->update($values);
        return $row->id;
    }

    public function delete($id)
    {
        $row = $this->getTable()->get($id);
        $row->delete($id);
        return $row->id;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->getTable()->count();
    }


    public function getPaginatorLimit($limit, $offset)
    {
        return $this->getTable()->limit($limit, $offset)->fetchAll();
    }

}
